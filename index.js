// Node.js Routing with HTTP method

// CRUD Operations			HTTP Methods
	// C- reate				POST
	// R- ead				GET
	// U- pdate				PUT / PATCH
	// D- elete				DELETE
/*
		Not only can we get the request url endpoint to differentiate requests but we can also check for the request method.

		We can think of request methods as the request/client telling our server the action to take for their request.

		This way we don't need to have different endpoints for everything that our client wants to do.

		HTTP methods allow us to group and organize our routes.
		
		With this we can have the same endpoints but with different methods.

		HTTP methods are particularly and primarily concerned with CRUD operations.

		Common HTTP Methods:
		
		GET - Get method for request indicates that the client/request wants to retrieve or get data.

		POST - Post method for request indicates that the client/request wants to post data and create a document.

		PUT - Put method for request indicates that the client/request wants to input data and update a document.

		DELETE - Delete method for request indicates that the client/request wants to delete a document.

	*/
const http = require('http');

// mock data
let users = [
	{
		username: 'peterIsHomeless',
		email: 'peterParker@mail.com',
		password: 'peterNoWayHome'
	},
	{
		username: 'Tony3000',
		email: 'starksIndustries@mail.com',
		password: 'ironManWillBeBack'
	}

];

let courses = [
	{
		name: 'Math 103',
		price: 2500,
		isActive: true
	},
	{
		name: 'Biology 201',
		price: 2500,
		isActive: true
	}

];

	http.createServer((req, res) => {

		// The HTTP method of the incoming request can be accessed via the "method" property of the "request" parameter
		// The method "GET" means that we will be retrieving or reading information
		if(req.url === '/' && req.method === 'GET'){

			res.writeHead(200, {'Content-Type' : 'text/plain'})
			res.end('This route is for checking GET method')
		
		} else if(req.url === '/' && req.method === 'POST'){

			res.writeHead(200, {'Content-Type' : 'text/plain'})
			res.end('This route is for checking POST method')
		
		} else if(req.url === '/' && req.method === 'PUT'){

			res.writeHead(200, {'Content-Type' : 'text/plain'})
			res.end('This route is for checking PUT method')

		} else if(req.url === '/' && req.method === 'DELETE'){

			res.writeHead(200, {'Content-Type' : 'text/plain'})
			res.end('This route is for checking DELETE method')
		
		} else if(req.url === '/users' && req.method === 'GET'){

			// Change the value of Content-Type header if we are passing json as our server response: 'application/json'
			res.writeHead(200, {'Content-Type' : 'application/json'})

			// We cannot pass other data type as a response except for strings
			// To be able to pass the array of users, first we stringify the array as JSON
			res.end(JSON.stringify(users));
		
		} else if(req.url === '/courses' && req.method === 'GET'){

			res.writeHead(200, {'Content-Type' : 'application/json'})

			res.end(JSON.stringify(courses));
		
		} else if(req.url === '/users' && req.method === 'POST'){

			// Declare and intialize a "requestBody" variable to an empty string
			// This will act as a placeholder for the resource/data to be created later on
			let requestBody = '';

			// Receiving data from client to nodejs server requires 2 steps:

			// data step- this part will read the stream of data from our client and process the incoming data into the requestBody variable
			req.on('data', (data) => {

				console.log(data)

				// Assigns the data retrieved from the data stream to requestBody
				requestBody += data
			})

			// end step- will run once or after the request has been completely sent from our client
			req.on('end', () => {

				console.log(requestBody)

				requestBody = JSON.parse(requestBody)

				// Create a new object representing the new mock database record
				let newUser = {
					username: requestBody.username,
					email: requestBody.email,
					password: requestBody.password
				}

				// Add the new user into the mock database
				users.push(newUser);
				console.log(users);

				res.writeHead(200, {'Content-Type' : 'application/json'})
				res.end(JSON.stringify(users))
			})
		
		} else if(req.url === '/courses' && req.method === 'POST'){

			let requestBody = '';

			req.on('data', (data) => {

				console.log(data)

				requestBody += data
			})

			// end-step
			req.on('end', () => {

				requestBody = JSON.parse(requestBody)

				let newCourse = {
					name: requestBody.name,
					price: requestBody.price,
					isActive: requestBody.isActive
				}

				courses.push(newCourse)
				res.writeHead(200, {'Content-Type' : 'application/json'})
				res.end(JSON.stringify(courses))

			})
		}



	}).listen(4000)
	console.log('Server is running at port 4000');


	/*
		Mini- Activity : 4 mins

		>> Create a new route with GET method
		>> This route is on "/courses" endpoint
			>> status code: 200
			>> content type for json
			>> end with showing the courses array

		>> Send your output in Hangouts

	*/
